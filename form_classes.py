from flask import Flask, render_template, flash, request, redirect, url_for
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename
from werkzeug.datastructures import CombinedMultiDict
from wtforms import Form, TextField, RadioField, SelectField, TextAreaField, validators, StringField,\
     SubmitField
import decode_text
import logging
from local_defs import mydefs
import os
import time
from logging.handlers import RotatingFileHandler

class text_decoder(Form):
    mapFile_1 = SelectField(u'Code Type', choices=mydefs.IMAGE_TYPE)
    mapFile_2 = SelectField(u'Version', choices=mydefs.RELEASE)
    mapFile_3 = SelectField(u'Version', choices=mydefs.PATCH_LETTER)
    mapFile_4 = SelectField(u'Version', choices=mydefs.PATCH_SPECIAL)
    # mapfile = TextField('Mapfile:', validators=[validators.required()])
    file = FileField('File:')
    do_jira = RadioField('Jira Query', choices=[('on', 'on'), ('off', 'off')], default='off')
    # plat_override = SelectField(u'Platform', choices =mydefs.PLAT_TYPE, default='GUESS')

class text_decoder_nofile(Form):
    mapFile_1 = SelectField(u'Code Type', choices=mydefs.IMAGE_TYPE)
    mapFile_2 = SelectField(u'Version', choices=mydefs.RELEASE)
    mapFile_3 = SelectField(u'Version', choices=mydefs.PATCH_LETTER)
    mapFile_4 = SelectField(u'Version', choices=mydefs.PATCH_SPECIAL)
    do_jira = RadioField('Jira Query', choices=[('on', 'on'), ('off', 'off')], default='off')
    stack_output = TextAreaField('stack_output', validators=[validators.Required()])

class gen_script(Form):
    # script_lan = SelectField(u'Script Language', choices=mydefs.SCRIPT_LANGUAGES)
    target_ip = TextField('Target IP', default = '<Target ip here>', validators=[validators.Required()])
    username = TextField('username', default = '<username>')
    password = TextField('password', default = '<password>')
    enable = TextField('enable', default = '<enable password>')
    access_type = RadioField('Access Type:', choices=[('ssh', 'ssh'),\
                        ('telnet','telnet')], default='ssh')
    authen_t = RadioField('Authentication:',choices=[\
    ('implicit','Implicit'),('UserEnable','User Enable'),\
    ('NoneEnable','None Enable'),('None','None')], default='None')
    trig_type = RadioField('Trigger Type: ', choices=[\
    ('Text Pattern','Text Pattern'),('Conditional','Conditional')], default='Conditional')
    command_trig = TextField('Trigger Command: ', validators=[validators.Required()], default='show cpu')
    trig_value = TextField('Trigger Value:', default='50')
    command_list = TextField('Capture Commands (Triggered): ')
    command_trig_cap = TextField('Capture Commands (Non-triggered):')
    command_interval = TextField('command_interval', default='10')
    script_interval = TextField('script_interval', default='1')
